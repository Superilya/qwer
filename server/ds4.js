const readline = require('readline-promise');

const rlp = readline.default.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: true
});

const renderControllers = controllers => controllers.map((controller, index) => {
    if (isBluetoothHID(controller)) {
        return `${index}) Джойстик (Bluetooth)\n`
    }

    return `${index}) Джойстик\n`;
}).join('');

var hid = require('node-hid');
var ds4 = require('./parse');
const { parseDS4HIDData } = require('./parse');
let hidDevice = null;
let offset = 0;

const init = async () => {

    const dsDevices = hid.devices().filter(({ vendorId, productId }) => 1356 === vendorId && productId === 2508);

    console.log('dsDevices', dsDevices);
    const targetDs = await rlp.questionAsync(`
Какой джойстик?:
${renderControllers(dsDevices)}`);

    const controller = dsDevices[targetDs];
    hidDevice = new hid.HID(controller.path);

    if (isBluetoothHID(controller)) {
        offset = 2;

        hidDevice.getFeatureReport(0x04, 66);
    }
};

init();

exports.listen = async (leftFn, upFn, rightFn, downFn) => {
    let up = false;
    let down = false;
    let left = false;
    let right = false;

    hidDevice.on('data', function (buf) {
        const { dPadUp, dPadRight, dPadDown, dPadLeft } = parseDS4HIDData(buf.slice(offset));

        if (up === false && dPadUp === true) {
            upFn();
        }

        if (right === false && dPadRight === true) {
            rightFn();
        }

        if (down === false && dPadDown === true) {
            downFn();
        }

        if (left === false && dPadLeft === true) {
            leftFn();
        }

        up = dPadUp;
        down = dPadRight;
        left = dPadDown;
        right = dPadLeft;
    });
}

// HIDDesciptor -> Boolean
function isDS4HID(descriptor) {
    return descriptor.vendorId == 1356 && descriptor.productId == 1476;
}

// HIDDesciptor -> Boolean
function isBluetoothHID(descriptor) {
    return descriptor.path.includes('Bluetooth');
}

// HIDDesciptor -> Boolean
function isUSBHID(descriptor) {
    return descriptor.path.match(/^USB/);
}