const qwe = ['LEFT', 'RIGHT', 'UP', 'DOWN'];
const anti = {
    'LEFT': 'RIGHT',
    'RIGHT': 'LEFT',
    'UP': 'DOWN',
    'DOWN': 'UP'
};

const ds = require('./ds4');

module.exports = class Player {
    constructor(field, startPosition, players) {
        this.target = 'LEFT';

        ds.listen(
            () => this.target = 'LEFT',
            () => this.target = 'UP',
            () => this.target = 'RIGHT',
            () => this.target = 'DOWN'
        )
    }

    async tick(content) {
        return this.target;
    }

    async win() {
        console.log('WIN');
    }

    async dead() {
        console.log('DEAD');
    }
}
