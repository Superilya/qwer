const express = require('express');
const bodyParser = require('body-parser');

module.exports = class Client {
    constructor(port, Player) {
        this.port = port;
        this.gameCounter = 0;
        this.games = {};

        this.init = async (req, res) => {
            const { field, startPosition, players } = req.body;
            this.games[this.gameCounter] = new Player(field, startPosition, players);

            const move = await this.games[this.gameCounter].tick({});

            res.json({ id: this.gameCounter, move });
            ++this.gameCounter;
        };

        this.tick = async (req, res) => {
            const player = this.games[req.params.gameId];
            const { actions } = req.body;

            const move = await player.tick(actions);

            res.json({ move })
        };

        this.dead = async (req, res) => {
            const player = this.games[req.params.gameId];

            await player.dead(req.body);
            res.end();
        };

        this.win = async (req, res) => {
            const player = this.games[req.params.gameId];

            await player.win(req.body);
            res.end();
        };

        this.app = express();

        this.app.use(bodyParser.json());
        this.app.post('/init', this.init);
        this.app.post('/tick/:gameId', this.tick);
        this.app.post('/dead/:gameId', this.dead);
        this.app.post('/win/:gameId', this.win);

        this.app.listen(this.port, () => {
            console.log(`App listening on port ${this.port}!`)
        });
    }
};
