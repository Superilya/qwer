export const CELL_WIDTH = 60;
export const CELL_HEIGHT = 60;

export const UP = 'UP';
export const DOWN = 'DOWN';
export const LEFT = 'LEFT';
export const RIGHT = 'RIGHT';

export const RED = 'RED';
export const BLUE = 'BLUE';
export const PURPURE = 'PURPURE';
export const GREEN = 'GREEN';

export const STATE_START_GAME = 0;
export const STATE_GAME = 1;
export const STATE_REPEAT = 2;
