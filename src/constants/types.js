export const INIT = 'INIT';
export const TICK = 'TICK';
export const RUN_GAME = 'RUN_GAME';
export const REPEAT_GAME = 'REPEAT_GAME';
export const FINAL_GAME = 'FINAL_GAME';
