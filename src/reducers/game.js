import {
    FINAL_GAME,
    INIT
} from 'src/constants/types';

const initState = {
    initialized: false
};

export default (state = initState, action) => {
    switch (action.type) {
        case INIT: {
            return {
                ...state,
                initialized: true
            };
        }

        case FINAL_GAME: {
            return {
                ...state,
                initialized: false
            }
        }

        default: return state;
    }
}