import {
    INIT
} from 'src/constants/types';

const initState = {
    width: null,
    height: null
};

export default (state = initState, action) => {
    switch (action.type) {
        case INIT: {
            return {
                ...state,
                width: action.size.width,
                height: action.size.height
            };
        }

        default: return state;
    }
}