import {
    INIT, TICK
} from 'src/constants/types';

const initState = {
    content: null
};

export default (state = initState, action) => {
    switch (action.type) {
        case TICK:
        case INIT: {
            return {
                content: action.marks
            };
        }

        default: return state;
    }
}