import { combineReducers } from 'redux';
import size from './size';
import marks from './marks';

export default combineReducers({
    size,
    marks
});
