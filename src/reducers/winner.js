import {
    FINAL_GAME
} from 'src/constants/types';

const initState = null;

export default (state = initState, action) => {
    switch (action.type) {
        case FINAL_GAME: {
            return action.winner;
        }

        default: return state;
    }
}