import {
    INIT
} from 'src/constants/types';

const initState = {};

export default (state = initState, action) => {
    switch (action.type) {
        case INIT: {
            return {
                ...state,
                ...Object.keys(action.fighters).reduce((acc, playerId) => {
                    acc[playerId] = action.fighters[playerId].position;

                    return acc;
                }, {})
            };
        }

        default: return state;
    }
}