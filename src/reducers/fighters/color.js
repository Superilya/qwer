import {
    INIT
} from 'src/constants/types';
import { RED, GREEN, BLUE, PURPURE } from 'src/constants';

const map = [BLUE, RED, PURPURE, GREEN];
const initState = {};

export default (state = initState, action) => {
    switch (action.type) {
        case INIT: {
            return Object.keys(action.fighters).reduce((acc, fighter, index) => {
                const colorNumber = index % map.length;

                acc[fighter] = map[colorNumber];

                return acc;
            }, { ...state });
        }

        default: return state;
    }
};
