import {
    INIT,
    TICK
} from 'src/constants/types';

const initState = {};

export default (state = initState, action) => {
    switch (action.type) {
        case INIT:
        case TICK: {
            return { ...action.positions };
        }

        default: return state;
    }
}