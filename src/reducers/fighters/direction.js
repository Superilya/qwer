import {
    TICK
} from 'src/constants/types';

const initState = {};

export default (state = initState, action) => {
    switch (action.type) {
        case TICK: {
            return {
                ...state,
                ...action.moves
            }
        }

        default: return state;
    }
};
