import {
    TICK
} from 'src/constants/types';

const initState = {};

export default (state = initState, action) => {
    switch (action.type) {
        case TICK: {
            return Object.keys(action.moves).reduce((acc, fighter) => {
                if (Array.isArray(acc[fighter])) {
                    acc[fighter] = [...acc[fighter], action.moves[fighter]];
                } else {
                    acc[fighter] = [action.moves[fighter]];
                }

                return acc;
            }, { ...state });
        }

        default: return state;
    }
};
