import { combineReducers } from 'redux';
import position from './position';
import direction from './direction';
import color from './color';
import startPosition from './start-position';
import way from './way';

export default combineReducers({
    position,
    color,
    direction,
    startPosition,
    way
});
