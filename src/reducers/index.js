import { combineReducers } from 'redux';
import game from './game';
import field from './field';
import fighters from './fighters';
import state from './state';
import winner from './winner';

export default combineReducers({
    field,
    fighters,
    game,
    state,
    winner
});
