import {
    FINAL_GAME,
    REPEAT_GAME,
    RUN_GAME,
} from 'src/constants/types';
import {
    STATE_START_GAME,
    STATE_GAME,
    STATE_REPEAT
} from 'src/constants';

const initState = STATE_START_GAME;

export default (state = initState, action) => {
    switch (action.type) {
        case RUN_GAME: {
            return STATE_GAME;
        }

        case FINAL_GAME: {
            return STATE_REPEAT;
        }

        case REPEAT_GAME: {
            return STATE_START_GAME;
        }

        default: return state;
    }
}