import {
    INIT,
    TICK,
    REPEAT_GAME,
    RUN_GAME,
    FINAL_GAME
} from 'src/constants/types';

export const init = ({ size, fighters, marks, positions }) => ({
    type: INIT,
    fighters,
    size,
    marks,
    positions
});

export const tick = ({ moves, marks, positions }) => ({
    type: TICK,
    moves,
    marks,
    positions
});

export const runGame = ({ field, players }) => ({
    type: RUN_GAME,
    field,
    players
});

export const repeatGame = () => ({
    type: REPEAT_GAME
});

export const finalGame = ({ winner }) => ({
    type: FINAL_GAME,
    winner
});