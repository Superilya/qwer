import React, { Component } from 'react';
import { Provider } from 'react-redux';
import configureStore from 'src/store';
import { Page } from 'src/components/page';
import './style.css';

const store = configureStore();

export const App = () => (
    <Provider store={store}>
        <Page />
    </Provider>
);
