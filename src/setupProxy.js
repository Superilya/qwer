const proxy = require('http-proxy-middleware');

module.exports = function(app) {
    app.use('/client1', proxy(
        {
            target: 'http://localhost:3001/',
            pathRewrite: {
                '^/client1': '/'
            }
        }
    ));

    app.use('/client2', proxy(
        {
            target: 'http://localhost:3002/',
            pathRewrite: {
                '^/client2': '/'
            }
        }
    ));

    app.use('/client3', proxy(
        {
            target: 'http://localhost:3003/',
            pathRewrite: {
                '^/client3': '/'
            }
        }
    ));

    app.use('/client4', proxy(
        {
            target: 'http://localhost:3004/',
            pathRewrite: {
                '^/client4': '/'
            }
        }
    ));

    app.use('/client5', proxy(
        {
            target: 'http://localhost:3005/',
            pathRewrite: {
                '^/client5': '/'
            }
        }
    ));
};
