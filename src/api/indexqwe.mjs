import fetch from 'isomorphic-fetch';
import cloneDeep from 'lodash/cloneDeep';
import merge from 'lodash/merge';

const directions = ['LEFT', 'RIGHT', 'UP', 'DOWN'];

export default class Game {
    constructor(options) {
        this.players = options.players.reduce((acc, player, index) => {
            const targetPlayer = cloneDeep(player);
            acc[index] = targetPlayer;

            return acc;
        }, {});
        this.field = options.field;
        this.games = {};
        this.marks = {};
        this.end = false;
        this.positions = options.players.reduce((acc, player, index) => {
            acc[index] = player.startPosition;

            return acc;
        }, {});

        this.init = function * () {
            const playerIds = Object.keys(this.players);
            const moves = {};

            for (let i = 0; i < playerIds.length; ++i) {
                const playerId = playerIds[i];
                const player = this.players[playerId];
                const otherPlayersIds = Object.keys(this.players).filter(id => id !== playerId);

                const response = yield this.fetch(player.host + '/init', {
                    field: this.field,
                    startPosition: player.startPosition,
                    players: otherPlayersIds.reduce((acc, id) => {
                        acc[id] = { startPosition: this.players[id].startPosition };

                        return acc;
                    }, {})
                });

                if (typeof response !== 'object') {
                    throw new Error(`Invalid init response, host - ${player.host}`);
                }

                const gameId = Number(response.id);

                if (isNaN(gameId)) {
                    throw new Error(`Invalid game id, host - ${player.host}`);
                }

                if (!directions.includes(response.move)) {
                    throw new Error(`Invalid first move, host - ${player.host}`);
                }

                this.games[playerId] = gameId;
                moves[playerId] = response.move;
            }

            this.run(moves);

            return this.status();
        }.bind(this);

        this.tick = function * () {
            if (!this.games || this.end) {
                return;
            }

            const playerIds = Object.keys(this.players);
            const moves = {};
            const live = [];

            for (let i = 0; i < playerIds.length; ++i) {
                const playerId = playerIds[i];
                const player = this.players[playerId];
                const gameId = this.games[playerId];
                const actions = Object.keys(this.lastMoves).filter(id => id !== playerId).reduce((acc, playerId) => {
                    acc[playerId] = this.lastMoves[playerId];

                    return acc;
                }, {});

                if (!player.dead) {
                    const response = yield this.fetch(`${player.host}/tick/${gameId}`, {
                        actions
                    });

                    if (typeof response !== 'object') {
                        throw new Error(`Invalid tick response, player host - ${player.host}`);
                    }

                    if (!directions.includes(response.move)) {
                        throw new Error(`Invalid tick move, player host - ${player.host}`);
                    }

                    moves[playerId] = response.move;
                    live.push(playerId);
                } else if (!player.sendDead) {
                    yield this.fetch(`${player.host}/dead/${gameId}`, {
                        actions
                    });

                    player.sendDead = true;
                }
            }

            if (live.length === 1) {
                const [playerId] = live;
                const gameId = this.games[playerId];
                const player = this.players[playerId];

                yield this.fetch(`${player.host}/win/${gameId}`);
            }

            if (live.length <= 1) {
                this.end = true;
            }

            this.run(moves);

            return cloneDeep(this.lastMoves);
        }.bind(this);
    }

    fetch(url, body) {
        return fetch(
            url,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(body)
            }
        )
            .then(response => {
                return response.text();
            })
            .then(text => {
                if (text) {
                    return JSON.parse(text);
                }

                return null;
            })
    }

    run(moves) {
        const newMarks = {};

        Object.keys(moves).forEach(playerId => {
            this.players[playerId].move = moves[playerId];

            if (!this.players[playerId].dead) {
                const targetPosition = this.getTargetPosition(this.positions[playerId], moves[playerId]);

                if (this.checkMarks(this.marks, targetPosition)) {
                    this.players[playerId].dead = true;

                    return;
                }

                this.mark(newMarks, this.positions[playerId], field => {
                    field.to = moves[playerId];
                    field.playerId = playerId;
                });

                this.mark(newMarks, targetPosition, field => {
                    field.from = moves[playerId];
                    field.playerId = playerId;
                });

                this.positions[playerId] = targetPosition;
            }
        });

        this.lastMoves = moves;

        this.marks = merge(this.marks, newMarks);
    }

    checkMarks(marks, position) {
        const xLine = marks[position.left];

        if (!xLine) {
            return false;
        }

        const yLine = xLine[position.top];

        return Boolean(yLine);
    }

    mark(marks, position, marker) {
        if (!marks[position.left]) {
            marks[position.left] = {};
        }

        if (!marks[position.left][position.top]) {
            marks[position.left][position.top] = {};
        }

        marker(marks[position.left][position.top]);
    }

    getTargetPosition(position, direction) {
        const targetPosition = { ...position };

        switch (direction) {
            case 'LEFT': {
                if (targetPosition.left <= 0) {
                    targetPosition.left = this.field.width - 1;
                } else {
                    targetPosition.left -= 1;
                }

                return targetPosition;
            }

            case 'RIGHT': {
                if (targetPosition.left >= this.field.width - 1) {
                    targetPosition.left = 0;
                } else {
                    targetPosition.left += 1;
                }

                return targetPosition;
            }

            case 'UP': {
                if (targetPosition.top <= 0) {
                    targetPosition.top = this.field.height - 1;
                } else {
                    targetPosition.top -= 1;
                }

                return targetPosition;
            }

            case 'DOWN': {
                if (targetPosition.top >= this.field.height - 1) {
                    targetPosition.top = 0;
                } else {
                    targetPosition.top += 1;
                }

                return targetPosition;
            }

            default: {
                return targetPosition;
            }
        }
    }

    status() {
        return cloneDeep(this.players);
    }
}