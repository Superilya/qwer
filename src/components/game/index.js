import { connect } from 'react-redux';
import { GameView } from './view';

const mapStateToProps = ({ game, fighters }) => ({
    initialized: game.initialized,
    fighters: Object.keys(fighters.position)
});

export const Game = connect(mapStateToProps)(GameView);
