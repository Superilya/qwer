import React from 'react';
import { Field } from 'src/components/field';
import { Fighter } from 'src/components/fighter';
import { Way } from 'src/components/way';

export const GameView = ({ initialized, fighters }) => {
    if (!initialized) {
        return <div className='field-wrap' />;
    }

    return (
        <div className='field-wrap'>
            <Field>
                {fighters.map(fighter => (
                    <Way key={fighter} id={fighter} />
                ))}
                {fighters.map(fighter => (
                    <Fighter key={fighter} id={fighter} />
                ))}
            </Field>
        </div>
    );
};
