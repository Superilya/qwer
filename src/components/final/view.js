import React, { Fragment, Component } from 'react';
import { BLUE, GREEN, PURPURE, RED } from 'src/constants';

export class FinalView extends Component {
    render() {
        return (
            <Fragment>
                <div className="layout__cloud" />
                <div className="layout__logo">
                    <div className="logo">
                        <div className="logo__name" />
                        <div className="logo__ground" />
                        <div className="logo__grass logo__grass_place_one" />
                        <div className="logo__grass logo__grass_place_two" />
                        <div className="logo__grass logo__grass_place_three" />
                        <div className="logo__grass logo__grass_place_four" />
                    </div>
                </div>
                <div className="layout__content">
                    <div className="layout__work-space form-wrap">
                        <div className="form-wrap__inner form">
                            <div className="form__field align-center">
                                <span className="text text_color_white text_size_big">{this.renderLabel()}</span>
                            </div>
                            <div className="form__field align-center">
                                <button className="button" onClick={this.handleRepeat}>
                                    Ещё раз
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }

    renderLabel() {
        const { winner } = this.props;

        switch (winner) {
            case BLUE: {
                return 'Выиграл синий';
            }
            case RED: {
                return 'Выиграл красный';
            }
            case PURPURE: {
                return 'Выиграл фиолетовый';
            }
            case GREEN: {
                return 'Выиграл зелёный';
            }

            default: return 'Ничья'
        }
    }

    handleRepeat = () => {
        const { repeatGame } = this.props;

        repeatGame();
    }
}