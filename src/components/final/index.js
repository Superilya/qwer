import { FinalView } from './view';
import { repeatGame } from 'src/actions';
import { connect } from 'react-redux';

const mapStateToProps = ({ winner }) => ({
    winner
});

export const Final = connect(mapStateToProps, { repeatGame })(FinalView);