import { WayView } from './view';
import { connect } from 'react-redux';

const mapStateToProps = ({ field, fighters }) => ({
    marks: field.marks,
    colors: fighters.color
});

export const Way = connect(mapStateToProps)(WayView);