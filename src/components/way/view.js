import React, { Fragment, Component } from 'react';
import './style.css';
import { BLUE, GREEN, PURPURE, RED } from 'src/constants';

import RightBlue from './resources/blue/finish_right.png'
import LeftBlue from './resources/blue/finish_left.png';
import UpBlue from './resources/blue/finish_up.png';
import DownBlue from './resources/blue/finish_down.png';
import LeftRightBlue from './resources/blue/horizontal.png';
import UpDownBlue from './resources/blue/vertical.png';
import LeftUpBlue from './resources/blue/left_up.png';
import UpRightBlue from './resources/blue/up_right.png';
import RightDownBlue from './resources/blue/right_down.png';
import LeftDownBlue from './resources/blue/down_left.png';

import RightGreen from './resources/green/finish_right.png'
import LeftGreen from './resources/green/finish_left.png';
import UpGreen from './resources/green/finish_up.png';
import DownGreen from './resources/green/finish_down.png';
import LeftRightGreen from './resources/green/horizontal.png';
import UpDownGreen from './resources/green/vertical.png';
import LeftUpGreen from './resources/green/left_up.png';
import UpRightGreen from './resources/green/up_right.png';
import RightDownGreen from './resources/green/right_down.png';
import LeftDownGreen from './resources/green/down_left.png';

import RightPurpure from './resources/purpure/finish_right.png'
import LeftPurpure from './resources/purpure/finish_left.png';
import UpPurpure from './resources/purpure/finish_up.png';
import DownPurpure from './resources/purpure/finish_down.png';
import LeftRightPurpure from './resources/purpure/horizontal.png';
import UpDownPurpure from './resources/purpure/vertical.png';
import LeftUpPurpure from './resources/purpure/left_up.png';
import UpRightPurpure from './resources/purpure/up_right.png';
import RightDownPurpure from './resources/purpure/right_down.png';
import LeftDownPurpure from './resources/purpure/down_left.png';

import RightRed from './resources/red/finish_right.png'
import LeftRed from './resources/red/finish_left.png';
import UpRed from './resources/red/finish_up.png';
import DownRed from './resources/red/finish_down.png';
import LeftRightRed from './resources/red/horizontal.png';
import UpDownRed from './resources/red/vertical.png';
import LeftUpRed from './resources/red/left_up.png';
import UpRightRed from './resources/red/up_right.png';
import RightDownRed from './resources/red/right_down.png';
import LeftDownRed from './resources/red/down_left.png';

import { RIGHT, UP, DOWN, LEFT, CELL_WIDTH, CELL_HEIGHT } from 'src/constants';

const map = {
    [BLUE]: {
        [LEFT]: {
            [LEFT]: LeftRightBlue,
            [UP]: UpRightBlue,
            [DOWN]: RightDownBlue
        },
        [UP]: {
            [RIGHT]: RightDownBlue,
            [UP]: UpDownBlue,
            [LEFT]: LeftDownBlue
        },
        [RIGHT]: {
            [UP]: LeftUpBlue,
            [DOWN]: LeftDownBlue,
            [RIGHT]: LeftRightBlue
        },
        [DOWN]: {
            [LEFT]: LeftUpBlue,
            [DOWN]: UpDownBlue,
            [RIGHT]: UpRightBlue
        }
    },
    [GREEN]: {
        [LEFT]: {
            [LEFT]: LeftRightGreen,
            [UP]: UpRightGreen,
            [DOWN]: RightDownGreen
        },
        [UP]: {
            [RIGHT]: RightDownGreen,
            [UP]: UpDownGreen,
            [LEFT]: LeftDownGreen
        },
        [RIGHT]: {
            [UP]: LeftUpGreen,
            [DOWN]: LeftDownGreen,
            [RIGHT]: LeftRightGreen
        },
        [DOWN]: {
            [LEFT]: LeftUpGreen,
            [DOWN]: UpDownGreen,
            [RIGHT]: UpRightGreen
        }
    },
    [RED]: {
        [LEFT]: {
            [LEFT]: LeftRightRed,
            [UP]: UpRightRed,
            [DOWN]: RightDownRed
        },
        [UP]: {
            [RIGHT]: RightDownRed,
            [UP]: UpDownRed,
            [LEFT]: LeftDownRed
        },
        [RIGHT]: {
            [UP]: LeftUpRed,
            [DOWN]: LeftDownRed,
            [RIGHT]: LeftRightRed
        },
        [DOWN]: {
            [LEFT]: LeftUpRed,
            [DOWN]: UpDownRed,
            [RIGHT]: UpRightRed
        }
    },
    [PURPURE]: {
        [LEFT]: {
            [LEFT]: LeftRightPurpure,
            [UP]: UpRightPurpure,
            [DOWN]: RightDownPurpure
        },
        [UP]: {
            [RIGHT]: RightDownPurpure,
            [UP]: UpDownPurpure,
            [LEFT]: LeftDownPurpure
        },
        [RIGHT]: {
            [UP]: LeftUpPurpure,
            [DOWN]: LeftDownPurpure,
            [RIGHT]: LeftRightPurpure
        },
        [DOWN]: {
            [LEFT]: LeftUpPurpure,
            [DOWN]: UpDownPurpure,
            [RIGHT]: UpRightPurpure
        }
    }
};

const fromMap = {
    [BLUE]: {
        [UP]: DownBlue,
        [RIGHT]: LeftBlue,
        [LEFT]: RightBlue,
        [DOWN]: UpBlue
    },
    [GREEN]: {
        [UP]: DownGreen,
        [RIGHT]: LeftGreen,
        [LEFT]: RightGreen,
        [DOWN]: UpGreen
    },
    [RED]: {
        [UP]: DownRed,
        [RIGHT]: LeftRed,
        [LEFT]: RightRed,
        [DOWN]: UpRed
    },
    [PURPURE]: {
        [UP]: DownPurpure,
        [RIGHT]: LeftPurpure,
        [LEFT]: RightPurpure,
        [DOWN]: UpPurpure
    }
};

const toMap = {
    [BLUE]: {
        [UP]: UpBlue,
        [RIGHT]: RightBlue,
        [LEFT]: LeftBlue,
        [DOWN]: DownBlue
    },
    [GREEN]: {
        [UP]: UpGreen ,
        [RIGHT]: RightGreen,
        [LEFT]: LeftGreen,
        [DOWN]: DownGreen
    },
    [RED]: {
        [UP]: UpRed,
        [RIGHT]: RightRed,
        [LEFT]: LeftRed,
        [DOWN]: DownRed
    },
    [PURPURE]: {
        [UP]: UpPurpure ,
        [RIGHT]: RightPurpure,
        [LEFT]: LeftPurpure,
        [DOWN]: DownPurpure
    }
};

const updatePisition = (startPosition, direction) => {
    const position = { ...startPosition };

    switch (direction) {
        case LEFT: {
            position.left -= 1;
            return position;
        }

        case RIGHT: {
            position.left += 1;
            return position;
        }

        case UP: {
            position.top -= 1;
            return position;
        }

        case DOWN: {
            position.top += 1;
            return position;
        }
    }

    return position;
};

export class WayView extends Component {
    render() {
        const { marks } = this.props;
        const { content } = marks;
        const result = [];

        if (content) {
            Object.keys(content).forEach(y => {
                Object.keys(content[y]).forEach(x => {
                    const field = content[y][x];

                    result.push(
                        <img
                            className='way_cell'
                            style={{
                                left: y * CELL_WIDTH,
                                top: x * CELL_HEIGHT,
                                width: CELL_WIDTH,
                                height: CELL_HEIGHT
                            }}
                            src={this.getSrc(field)}/>
                    )
                });
            });
        }

        return (
            <Fragment>
                {result}
            </Fragment>
        );
    }

    getSrc(field) {
        const { from, to, playerId } = field;
        const { colors } = this.props;
        const color = colors[playerId] || BLUE;

        if (!from) {
            return toMap[color][to];
        }

        if (!to) {
            return fromMap[color][from];
        }

        return map[color][from][to];
    }
};