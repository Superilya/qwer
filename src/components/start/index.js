import { connect } from 'react-redux';
import { runGame } from 'src/actions';
import { StartView } from './view';

export const Start = connect(null, { runGame })(StartView);