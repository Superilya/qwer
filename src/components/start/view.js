import React, { Component, Fragment } from 'react';
import { BLUE, RED, GREEN, PURPURE } from 'src/constants';

export class StartView extends Component {
    state = {
        width: 14,
        height: 14,
        bluePort: 1,
        blueLeft: 1,
        blueTop: 1,
        redPort: 2,
        redLeft: 10,
        redTop: 10,
        greenPort: 4,
        greenLeft: 5,
        greenTop: 5,
        purpurePort: 3,
        purpureLeft: 7,
        purpureTop: 7
    };

    render() {
        const {
            width,
            height,
            bluePort,
            blueLeft,
            blueTop,
            redPort,
            redLeft,
            redTop,
            greenPort,
            greenLeft,
            greenTop,
            purpurePort,
            purpureLeft,
            purpureTop
        } = this.state;

        return (
            <Fragment>
                <div className="layout__cloud" />
                <div className="layout__logo">
                    <div className="logo">
                        <div className="logo__name" />
                        <div className="logo__ground" />
                        <div className="logo__grass logo__grass_place_one" />
                        <div className="logo__grass logo__grass_place_two" />
                        <div className="logo__grass logo__grass_place_three" />
                        <div className="logo__grass logo__grass_place_four" />
                    </div>
                </div>
                <div className="layout__content">
                    <div className="layout__work-space form-wrap">
                        <div className="form-wrap__inner form">
                            <div className="form__field align-center">
                                <span className="text text_color_white text_size_big">Поле</span>
                            </div>
                            <div className="form__field">
                                <label className="input">
                                    <span className="input__label text text_color_green">Высота</span>
                                    <input
                                        className="input__field"
                                        placeholder="14"
                                        value={height}
                                        name='height'
                                        onChange={this.handleChange} />
                                </label>
                            </div>
                            <div className="form__field">
                                <label className="select select_wide">
                                    <span className="input__label text text_color_green">Ширина</span>
                                    <input
                                        className="input__field"
                                        placeholder="14"
                                        value={width}
                                        name='width'
                                        onChange={this.handleChange}/>
                                </label>
                            </div>
                            <div className="form__field align-center">
                                <span className="text text_color_white text_size_big">Синияя улитка</span>
                            </div>
                            <div className="form__field">
                                <label className="input">
                                    <span className="input__label text text_color_green">Порт</span>
                                    <input
                                        className="input__field"
                                        placeholder="3001"
                                        value={bluePort}
                                        name='bluePort'
                                        onChange={this.handleChange} />
                                </label>
                            </div>
                            <div className="form__field">
                                <label className="select select_wide">
                                    <span className="input__label text text_color_green">left</span>
                                    <input
                                        className="input__field"
                                        placeholder="14"
                                        value={blueLeft}
                                        name='blueLeft'
                                        onChange={this.handleChange}/>
                                </label>
                            </div>
                            <div className="form__field">
                                <label className="select select_wide">
                                    <span className="input__label text text_color_green">top</span>
                                    <input
                                        className="input__field"
                                        placeholder="14"
                                        value={blueTop}
                                        name='blueTop'
                                        onChange={this.handleChange}/>
                                </label>
                            </div>
                            <div className="form__field align-center">
                                <span className="text text_color_white text_size_big">Красная улитка</span>
                            </div>
                            <div className="form__field">
                                <label className="input">
                                    <span className="input__label text text_color_green">Порт</span>
                                    <input
                                        className="input__field"
                                        placeholder="3001"
                                        value={redPort}
                                        name='redPort'
                                        onChange={this.handleChange} />
                                </label>
                            </div>
                            <div className="form__field">
                                <label className="select select_wide">
                                    <span className="input__label text text_color_green">left</span>
                                    <input
                                        className="input__field"
                                        placeholder="14"
                                        value={redLeft}
                                        name='redLeft'
                                        onChange={this.handleChange}/>
                                </label>
                            </div>
                            <div className="form__field">
                                <label className="select select_wide">
                                    <span className="input__label text text_color_green">top</span>
                                    <input
                                        className="input__field"
                                        placeholder="14"
                                        value={redTop}
                                        name='redTop'
                                        onChange={this.handleChange}/>
                                </label>
                            </div>
                            <div className="form__field align-center">
                                <span className="text text_color_white text_size_big">Фиолетовая улитка</span>
                            </div>
                            <div className="form__field">
                                <label className="input">
                                    <span className="input__label text text_color_green">Порт</span>
                                    <input
                                        className="input__field"
                                        placeholder="3001"
                                        value={purpurePort}
                                        name='purpurePort'
                                        onChange={this.handleChange} />
                                </label>
                            </div>
                            <div className="form__field">
                                <label className="select select_wide">
                                    <span className="input__label text text_color_green">left</span>
                                    <input
                                        className="input__field"
                                        placeholder="14"
                                        value={purpureLeft}
                                        name='purpureLeft'
                                        onChange={this.handleChange}/>
                                </label>
                            </div>
                            <div className="form__field">
                                <label className="select select_wide">
                                    <span className="input__label text text_color_green">top</span>
                                    <input
                                        className="input__field"
                                        placeholder="14"
                                        value={purpureTop}
                                        name='purpureTop'
                                        onChange={this.handleChange}/>
                                </label>
                            </div>
                            <div className="form__field align-center">
                                <span className="text text_color_white text_size_big">Зелёная улитка</span>
                            </div>
                            <div className="form__field">
                                <label className="input">
                                    <span className="input__label text text_color_green">Порт</span>
                                    <input
                                        className="input__field"
                                        placeholder="3001"
                                        value={greenPort}
                                        name='greenPort'
                                        onChange={this.handleChange} />
                                </label>
                            </div>
                            <div className="form__field">
                                <label className="select select_wide">
                                    <span className="input__label text text_color_green">left</span>
                                    <input
                                        className="input__field"
                                        placeholder="14"
                                        value={greenLeft}
                                        name='greenLeft'
                                        onChange={this.handleChange}/>
                                </label>
                            </div>
                            <div className="form__field">
                                <label className="select select_wide">
                                    <span className="input__label text text_color_green">top</span>
                                    <input
                                        className="input__field"
                                        placeholder="14"
                                        value={greenTop}
                                        name='greenTop'
                                        onChange={this.handleChange}/>
                                </label>
                            </div>
                            <div className="form__field align-center">
                                <button className="button" onClick={this.handleStart}>
                                    Играть
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }

    handleChange = e => {
        const { name, value } = e.currentTarget;

        this.setState({
            [name]: value
        });
    };

    handleStart = () => {
        const {
            width,
            height,
            bluePort,
            blueLeft,
            blueTop,
            redPort,
            redLeft,
            redTop,
            greenPort,
            greenLeft,
            greenTop,
            purpurePort,
            purpureLeft,
            purpureTop
        } = this.state;

        this.props.runGame({
            field: {
                width: Number(width),
                height: Number(height)
            },
            players: [
                bluePort && {
                    host: `/client${bluePort}`,
                    name: BLUE,
                    startPosition: {
                        left: blueLeft,
                        top: blueTop
                    }
                },
                redPort && {
                    host: `/client${redPort}`,
                    name: RED,
                    startPosition: {
                        left: redLeft,
                        top: redTop
                    }
                },
                purpurePort && {
                    host: `/client${purpurePort}`,
                    name: PURPURE,
                    startPosition: {
                        left: purpureLeft,
                        top: purpureTop
                    }
                },
                greenPort && {
                    host: `/client${greenPort}`,
                    name: GREEN,
                    startPosition: {
                        left: greenLeft,
                        top: greenTop
                    }
                }
            ].filter(Boolean)
        });
    }
}
