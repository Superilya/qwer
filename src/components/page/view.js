import React, { Component } from 'react';
import { Game } from 'src/components/game';
import { Start } from 'src/components/start';
import { Final } from 'src/components/final';
import { STATE_GAME, STATE_REPEAT } from 'src/constants';

export class PageView extends Component {
    render() {
        const { state } = this.props;

        switch (state) {
            case STATE_GAME: {
                return <Game />
            }

            case STATE_REPEAT: {
                return <Final />
            }

            default: {
                return <Start />
            }
        }
    }
}