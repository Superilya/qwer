import { PageView } from './view';
import { connect } from 'react-redux';

const mapStateToProps = ({ state }) => ({ state });

export const Page = connect(mapStateToProps)(PageView);