import { connect } from 'react-redux';
import { FieldView } from './view';

const mapStateToProps = ({ field }) => ({
    width: field.size.width,
    height: field.size.height
});

export const Field = connect(mapStateToProps)(FieldView);
