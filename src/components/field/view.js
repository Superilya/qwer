import React, { Component } from 'react';
import { CELL_HEIGHT, CELL_WIDTH } from 'src/constants';
import LeftTop from './resources/turn_up_left.png';
import RightTop from './resources/turn_right_up.png';
import RightBottom from './resources/turn_donw_right.png';
import LeftBottom from './resources/turn_left_down.png';
import Left from './resources/und_left.png';
import Top from './resources/und_up.png';
import Right from './resources/und_right.png';
import Bottom from './resources/und_down.png';
import Inner from './resources/ground.png';
import './style.css';

export class FieldView extends Component {
    render() {
        const { width, height, children } = this.props;

        return (
            <div className='field' style={{ width: (width + 2) * CELL_WIDTH, height: (height + 2) * CELL_HEIGHT }}>
                <img className='field_cell' height={CELL_HEIGHT} width={CELL_WIDTH} style={{ left: 0, top: 0 }} src={LeftTop} />
                <img className='field_cell' height={CELL_HEIGHT} width={CELL_WIDTH} style={{ right: 0, top: 0 }} src={RightTop} />
                <img className='field_cell' height={CELL_HEIGHT} width={CELL_WIDTH} style={{ right: 0, bottom: 0 }} src={RightBottom} />
                <img className='field_cell' height={CELL_HEIGHT} width={CELL_WIDTH} style={{ left: 0, bottom: 0 }} src={LeftBottom} />
                {this.renderLeftWall()}
                {this.renderTopWall()}
                {this.renderRightWall()}
                {this.renderBottomWall()}
                <div className='field_inner' style={{ left: CELL_WIDTH, top: CELL_HEIGHT, width: width * CELL_WIDTH, height: height * CELL_HEIGHT }}>
                    {this.renderInner()}
                    {children}
                </div>
            </div>
        )
    }

    renderInner() {
        const { width, height } = this.props;
        const result = [];

        for (let i = 0; i < width; ++i) {
            for (let j = 0; j < height; j++) {
                result.push(
                    <img
                        className='field_cell'
                        height={CELL_HEIGHT}
                        width={CELL_WIDTH}
                        style={{ left: i * CELL_WIDTH, top: j * CELL_HEIGHT }}
                        src={Inner} />
                )
            }
        }

        return result;
    }

    renderBottomWall() {
        const { width } = this.props;
        const result = [];

        for (let i = 0; i < width; ++i) {
            result.push(
                <img
                    className='field_cell'
                    height={CELL_HEIGHT}
                    width={CELL_WIDTH}
                    style={{ bottom: 0, left: CELL_WIDTH + i * CELL_WIDTH }}
                    src={Bottom} />
            );
        }

        return result;
    }

    renderRightWall() {
        const { height } = this.props;
        const result = [];

        for (let i = 0; i < height; ++i) {
            result.push(
                <img
                    className='field_cell'
                    height={CELL_HEIGHT}
                    width={CELL_WIDTH}
                    style={{ right: 0, top: CELL_HEIGHT + i * CELL_HEIGHT }}
                    src={Right} />
            );
        }

        return result;
    }

    renderTopWall() {
        const { width } = this.props;
        const result = [];

        for (let i = 0; i < width; ++i) {
            result.push(
                <img
                    className='field_cell'
                    height={CELL_HEIGHT}
                    width={CELL_WIDTH}
                    style={{ left: CELL_WIDTH + i * CELL_WIDTH, top: 0 }}
                    src={Top} />
            );
        }

        return result;
    }

    renderLeftWall() {
        const { height } = this.props;
        const result = [];

        for (let i = 0; i < height; ++i) {
            result.push(
                <img
                    className='field_cell'
                    height={CELL_HEIGHT}
                    width={CELL_WIDTH}
                    style={{ left: 0, top: CELL_HEIGHT + i * CELL_HEIGHT }}
                    src={Left} />
            );
        }

        return result;
    }
}
