import React, { Component, Fragment } from 'react';
import cn from 'classnames';
import { BLUE, CELL_HEIGHT, CELL_WIDTH, GREEN, PURPURE, RED } from 'src/constants';
import DownBlue from './resources/blue/down_1.png';
import Down1Blue from './resources/blue/down_2.png';
import UpBlue from './resources/blue/up_1.png';
import Up1Blue from './resources/blue/up_2.png';
import LeftBlue from './resources/blue/right_1.png';
import Left1Blue from './resources/blue/right_2.png';
import RightBlue from './resources/blue/left_1.png';
import Right1Blue from './resources/blue/left_2.png';

import DownGreen from './resources/green/down_1.png';
import Down1Green from './resources/green/down_2.png';
import UpGreen from './resources/green/up_1.png';
import Up1Green from './resources/green/up_2.png';
import LeftGreen from './resources/green/right_1.png';
import Left1Green from './resources/green/right_2.png';
import RightGreen from './resources/green/left_1.png';
import Right1Green from './resources/green/left_2.png';

import DownRed from './resources/red/down_1.png';
import Down1Red from './resources/red/down_2.png';
import UpRed from './resources/red/up_1.png';
import Up1Red from './resources/red/up_2.png';
import LeftRed from './resources/red/right_1.png';
import Left1Red from './resources/red/right_2.png';
import RightRed from './resources/red/left_1.png';
import Right1Red from './resources/red/left_2.png';

import DownPurpure from './resources/purpure/down_1.png';
import Down1Purpure from './resources/purpure/down_2.png';
import UpPurpure from './resources/purpure/up_1.png';
import Up1Purpure from './resources/purpure/up_2.png';
import LeftPurpure from './resources/purpure/right_1.png';
import Left1Purpure from './resources/purpure/right_2.png';
import RightPurpure from './resources/purpure/left_1.png';
import Right1Purpure from './resources/purpure/left_2.png';

import { UP, DOWN, LEFT, RIGHT } from 'src/constants';
import './style.css';

const src = {
    [GREEN]: {
        [UP]: {
            true: UpGreen,
            false: Up1Green
        },
        [DOWN]: {
            true: DownGreen,
            false: Down1Green
        },
        [LEFT]: {
            true: LeftGreen,
            false: Left1Green
        },
        [RIGHT]: {
            true: RightGreen,
            false: Right1Green
        }
    },
    [BLUE]: {
        [UP]: {
            true: UpBlue,
            false: Up1Blue
        },
        [DOWN]: {
            true: DownBlue,
            false: Down1Blue
        },
        [LEFT]: {
            true: LeftBlue,
            false: Left1Blue
        },
        [RIGHT]: {
            true: RightBlue,
            false: Right1Blue
        }
    },
    [PURPURE]: {
        [UP]: {
            true: UpPurpure,
            false: Up1Purpure
        },
        [DOWN]: {
            true: DownPurpure,
            false: Down1Purpure
        },
        [LEFT]: {
            true: LeftPurpure,
            false: Left1Purpure
        },
        [RIGHT]: {
            true: RightPurpure,
            false: Right1Purpure
        }
    },
    [RED]: {
        [UP]: {
            true: UpRed,
            false: Up1Red
        },
        [DOWN]: {
            true: DownRed,
            false: Down1Red
        },
        [LEFT]: {
            true: LeftRed,
            false: Left1Red
        },
        [RIGHT]: {
            true: RightRed,
            false: Right1Red
        }
    }
};

export class FighterView extends Component {
    state = {
        frame: false,
        intervalId: null
    };

    componentDidMount() {
        const intervalId = setInterval(this.toggleFrame.bind(this), 200);

        this.setState({
            intervalId
        });
    }

    componentWillUnmount() {
        const { intervalId } = this.state;

        clearInterval(intervalId);
    }

    toggleFrame() {
        const { frame } = this.state;

        this.setState({
            frame: !frame
        })
    }

    render() {
        const { left, top, width, height } = this.props;

        console.log('left, top', left, top, width, height);

        return (
            <Fragment>
                <img
                    className={cn('fighter', 'animate')}
                    style={{
                        left: left * CELL_WIDTH,
                        top: top * CELL_HEIGHT,
                        width: CELL_WIDTH,
                        height: CELL_HEIGHT
                    }}
                    src={this.getSrc()} />
            </Fragment>
        );
    }

    getSrc() {
        const { frame } = this.state;
        const { direction, color } = this.props;
        const directionPart = src[color][direction] || src[color][DOWN];

        return directionPart[frame];
    }
}