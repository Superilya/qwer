import { connect } from 'react-redux';
import { FighterView } from './view';

const mapStateToProps = ({ fighters, field }, { id }) => ({
    left: fighters.position[id].left,
    top: fighters.position[id].top,
    direction: fighters.direction[id],
    color: fighters.color[id],
    width: field.size.width,
    height: field.size.height
});

export const Fighter = connect(mapStateToProps)(FighterView);