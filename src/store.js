/* global window */

import { createStore, applyMiddleware, compose } from 'redux';
import reducers from 'src/reducers';
import createSagaMiddleware from 'redux-saga';
import saga from 'src/saga';

export default function configureStore(initialState = {}) {
    const sagaMiddleware = createSagaMiddleware();

    // eslint-disable-next-line no-underscore-dangle
    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : compose;

    const store = createStore(
        reducers,
        initialState,
        composeEnhancers(
            applyMiddleware(sagaMiddleware)
        )
    );

    sagaMiddleware.run(saga);

    return store;
}
