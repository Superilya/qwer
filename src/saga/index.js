import { takeLatest } from 'redux-saga/effects';
import { game } from './game';
import { RUN_GAME } from 'src/constants/types';

export default function * () {
    yield takeLatest(RUN_GAME, game);
}