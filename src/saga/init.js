import { put } from 'redux-saga/effects';
import { init } from 'src/actions';

export const initialize = function * () {
    const size = {
        width: 10,
        height: 10
    };

    const position = {
        one: {
            left: 5,
            top: 0
        },
        two: {
            left: 3,
            top: 3
        },
        tree: {
            left: 0,
            top: 0
        },
        four: {
            left: 9,
            top: 9
        }
    };

    yield put(init({ size, fighters: { position } }));
};
