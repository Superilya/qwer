import { put } from 'redux-saga/effects';
import { tick } from 'src/actions';
import { RIGHT, DOWN, LEFT, UP } from 'src/constants';
import { delay } from 'redux-saga';

export const tiks = function * () {
    yield delay(10);

    yield put(tick({
        moves: {
            one: DOWN,
            two: DOWN
        }
    }));

    yield delay(600);

    yield put(tick({
        moves: {
            one: DOWN,
            two: DOWN
        }
    }));

    yield delay(600);

    yield put(tick({
        moves: {
            one: RIGHT,
            two: RIGHT
        }
    }));

    yield delay(600);

    yield put(tick({
        moves: {
            one: RIGHT,
            two: RIGHT
        }
    }));

    yield delay(600);

    yield put(tick({
        moves: {
            one: UP,
            two: UP
        }
    }));

    yield delay(600);

    yield put(tick({
        moves: {
            one: UP,
            two: UP
        }
    }));

    yield delay(600);

    yield put(tick({
        moves: {
            one: LEFT,
            two: LEFT
        }
    }));

    yield delay(600);
};
