import Game from 'src/api';
import { call, put } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import { init, finalGame } from 'src/actions';
import { tick } from 'src/actions';

export const game = function * ({ field, players }) {
    try {
        const game = new Game({ field, players });
        const initStatus = yield call(() => game.init());

        yield put(init({ size: field, fighters: initStatus, marks: game.marks, positions: game.positions }));

        while (!game.end) {
            const moves = yield call(() => game.tick());

            yield put(tick({
                moves,
                marks: game.marks,
                positions: game.positions
            }));

            yield delay(600);
        }

        yield put(finalGame({ winner: game.end }));
    } catch (e) {
        console.log(e);
    }
};